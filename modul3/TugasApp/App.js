import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import LoginScreen from './tugas13/LoginScreen'
import SignUpScreen from './tugas13/SignUpScreen'
import AboutScreen from './tugas13/About'
import Tugas14 from './tugas14/App'
import Tugas15 from './tugas15/index'
import Tugas15_2 from './tugas15/tugas15-2/Index'

export default function App() {
  return (
    <LoginScreen/>,
    <SignUpScreen/>,
    <AboutScreen/>,
    <Tugas14/>,
    <Tugas15/>,
    <Tugas15_2/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
