import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import LoginScreen from './src/Login'
import AboutScreen from './src/About'
import HomeScreen from './src/Index'
import DetailScreen from './src/Detail'


export default function App() {
  return (
    //<LoginScreen/>
    //<AboutScreen/>
    <HomeScreen/>
    //<DetailScreen/>
    
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
